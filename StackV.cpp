#include "StackV.h"
#include <iostream>
#include <vector>

using namespace std;

//Stack::Stack();



int Stack::size()
{
return data.size();
}

void Stack::push(int value)
{
data.push_back(value);
}

void Stack::pop()
{
data.pop_back();
}

int Stack::top()
{
if(data.back()>-1)
	{
	return data.back();
	}
else
{
	cout << "Empty Stack!" <<endl;
}
}

void Stack::clear()
{
data.clear();
}
