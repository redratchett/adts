#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client"<<endl;

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
for(int i = 0;i < 69;i++)
{
L1.insert(i,1);
L2.insert(i+5,1);
}



cout << "List 1 size is "<< L1.size()<<endl;
cout << "List 2 size is " <<L2.size()<<endl;

cout << "The first element of List 2 is "<<L2.get(1)<<" but let's remove that"<<endl;
L2.remove(1);
cout << "The first element of List 2 is now "<<L2.get(1)<<endl;

cout << "Let's clear List 1"<<endl;
L1.clear();
cout << "List 1 is now ";
if(L1.size()==0)
{
cout << "empty."<<endl;
}
else
{
cout << L1.size() <<endl;
}
return 0;
}
