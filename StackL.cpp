#include "StackL.h"
#include <vector>
#include <iostream>




int Stack::size()
{
return data.size();
}

void Stack::push(int value)
{
data.insert(value,1);
}

void Stack::pop()
{
data.remove(1);
}

int Stack::top()
{
if(data.size() != 0)
	{
	return data.get(1);
	}
else
{
  cout << "Empty Stack!" <<endl;
}
}

void Stack::clear()
{
data.clear();
}
