#include "StackLL.h"
#include <iostream>
using namespace std;

class Stack::Node //self-referential Node class
{
	public:
	   int data = 0;
	   Node* link = nullptr;	
};



Stack::~Stack()
{
clear();
}

int Stack::size()
{
return num_elements;
}


void Stack::push(int val)
{
Node *newptr = new Node{val};
newptr->link = frontptr;
frontptr = newptr;
num_elements++;
}

void Stack::pop()
{
if(frontptr != nullptr)
{
Node *deleteptr = frontptr;
frontptr = frontptr->link;
delete deleteptr;
num_elements--;
}
else
{
cerr << "Stack Empty" <<endl;
}
}

int Stack::top()
{
if(frontptr != nullptr)
{
return frontptr->data;
}
else
{
cout << "Stack Empty" <<endl;
}
}

void Stack::clear()
{
Node* iPtr = frontptr;
while (iPtr != nullptr)
{
frontptr = frontptr->link;
delete iPtr;
iPtr = frontptr;
}
num_elements = 0;
}


